import socket as sock
from threading import Thread
from queue import Queue
from json import dumps, loads
from config import grid_server_address, H, W, trellis_i2c_addresses, button_server_address
from socketserver import UDPServer, BaseRequestHandler
from board import SCL, SDA, D13, D6
import busio
from adafruit_neotrellis.neotrellis import NeoTrellis
from adafruit_neotrellis.multitrellis import MultiTrellis
from time import sleep, perf_counter
from random import randint, random

print("Creating i2c bus")
i2c_bus = busio.I2C(SCL, SDA)
button_queue = Queue(100)

def make_cb():
    def button_cb(xcoord, ycoord, edge):
        if edge == NeoTrellis.EDGE_RISING:
            # print(xcoord, ycoord)
            button_queue.put((xcoord, ycoord))
        return
    return button_cb



print("Creating Trelli")
trelli = [ [], []]
for x, slice in enumerate(trellis_i2c_addresses):
    for y, addr in enumerate(slice):
        # print(addr, end=" ")
        t = NeoTrellis(i2c_bus, False, addr=addr)
        t.pixels.auto_write = False
        t.interrupt_enabled = True

        trelli[x].append(t)
        sleep(0.02)
print("Creating multiTrelli")
trellis = MultiTrellis(trelli)
print(trellis)
led_matrix = [[(0, 0, 0) for x in range(W)] for y in range(H)]
old_led_matrix = [[(0, 0, 0) for x in range(W)] for y in range(H)]
button_cb = make_cb()
print("Adding callbacks")
for y in range(H):
    for x in range(W):
        sleep(0.01)
        trellis.activate_key(x, y, NeoTrellis.EDGE_RISING)
        sleep(0.01)
        trellis.activate_key(x, y, NeoTrellis.EDGE_FALLING)
        trellis.set_callback(x, y, button_cb)
print("Done")

# def update_all(matrix):
#     for x in range(W):
#         for y in range(H):
#             trellis.color(x, y, (randint(2,10),randint(2,10),randint(2,10)))
#     for ts in trellis._trelli:
#         for t in ts:
#             t.pixels.show()
#     return

def update_grid(msg):
    updated_boards = set()
    for diff in msg.get('grid'):
        x, y, r, g, b = diff
        bx = int(x/4)
        by = int(y/4)
        updated_boards.add((bx,by))
        trellis.color(x, y, (r, g, b))
    for board in updated_boards:
        trellis._trelli[board[1]][board[0]].pixels.show()
    return

def grid_off():
    for x in range(W):
        for y in range(H):
            # print(x,y)
            trellis.color(x, y, (0,0,0))
    for ts in trellis._trelli:
        for t in ts:
            t.pixels.show()
    return


class myHandler(BaseRequestHandler):
    def setup(self):
        return
    def handle(self):
        msg = loads(self.request[0].strip())
        socket = self.request[1]
        if msg.get("close"):
            pass
        if msg == "off":
            grid_off()
        else:
            update_grid(msg)
            trellis.sync()

def grid_INT():
    trellis.sync()

# GPIO.setmode(GPIO.BCM)
INT_pin = 5
# GPIO.setup(INT_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
# GPIO.add_event_detect(INT_pin, GPIO.FALLING, callback=grid_INT, bouncetime=10)

def runserver():
    with UDPServer(grid_server_address, myHandler) as server:
        print(f"Grid server started: {server}")
        server.serve_forever()

server_thread = Thread(target=runserver, daemon=True)
server_thread.start()
print(f"thread started: {server_thread.name}")

button_sock = sock.socket(sock.AF_INET, sock.SOCK_DGRAM)

while True:
    if not button_queue.empty():
        updates = []
        while not button_queue.empty():
            updates.append(button_queue.get())
        button_sock.sendto(bytes(dumps(updates), "utf-8"), button_server_address)
    sleep(0.01)

