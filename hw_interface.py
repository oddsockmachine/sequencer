from distutils.command.config import config
import socket
from queue import Queue
from json import dumps, loads
from config import H, W
# from pykka import ThreadingActor
from threading import Thread
from time import sleep
from buses import bus_registry
from color_scheme import select_scheme
from constants import NOTE_ON, NOTE_OFF
from grid_client import Grid, Buttons
from encoder_client import Encoders, Dials

grid = Grid()
grid.start()

buttons = Buttons()
buttons.start()

dials = Dials()
dials.start()

enc = Encoders()
enc.start()

m = [[NOTE_OFF for x in range(H)] for y in range(W)]
bus = bus_registry.get('grid_led_bus')
bus.put(m)
sleep(0.1)

from random import randint
for y in range(8):
    for x in range(32):
        m[x][y] = NOTE_ON
        bus.put(m)
        sleep(1/60)

m = [[NOTE_OFF for x in range(H)] for y in range(W)]
bus.put(m)
sleep(0.5)

button_bus = bus_registry.get('grid_button_bus')
dial_bus = bus_registry.get('dial_bus')

while not button_bus.empty():
    print(button_bus.get())


while not dial_bus.empty():
    print(dial_bus.get())

enc.keep_running = False
grid.keep_running = False
buttons.keep_running = False
dials.keep_running = False

