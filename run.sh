# Execute a python script on the remote hardware
ssh -tt pi@sunrise.local "/home/pi/sequencer/venv/bin/python3 sequencer/$1"