from threading import Thread
from board import SCL, SDA, D13, D6
import busio
import digitalio
from adafruit_neotrellis.neotrellis import NeoTrellis
from adafruit_neotrellis.multitrellis import MultiTrellis
from time import sleep

H = 8
W = 32

print("Creating i2c bus")
i2c_bus = busio.I2C(SCL, SDA)




def make_cb():
    def button_cb(xcoord, ycoord, edge):
        if edge == NeoTrellis.EDGE_RISING:
            print(xcoord, ycoord)
        return
    return button_cb


trellis_addresses = [[0x31, 0x30, 0x2f, 0x2e, 0x36, 0x37, 0x38, 0x39],
                     [0x35, 0x34, 0x33, 0x32, 0x3a, 0x3c, 0x3b, 0x3d]]

trelli = [ [], []]

print("Creating Trelli...")
for x, slice in enumerate(trellis_addresses):
    for y, addr in enumerate(slice):
        print(addr)
        t = NeoTrellis(i2c_bus, False, addr=addr)
        t.pixels.auto_write = False
        trelli[x].append(t)
        sleep(0.05)
print("Creating multiTrelli")
trellis = MultiTrellis(trelli)
print(trellis)
led_matrix = [[(0, 0, 0) for x in range(W)] for y in range(H)]
old_led_matrix = [[(0, 0, 0) for x in range(W)] for y in range(H)]
button_cb = make_cb()
print("Adding callbacks")
for y in range(H):
    for x in range(W):
        sleep(0.01)
        trellis.activate_key(x, y, NeoTrellis.EDGE_RISING)
        sleep(0.01)
        trellis.activate_key(x, y, NeoTrellis.EDGE_FALLING)
        trellis.set_callback(x, y, button_cb)
print("Done")

from random import randint
import time
print("Setting colors")

print("Done")

# while True:
#     # The NeoTrellis can only be read every 17 milliseconds or so
#     trellis.sync()
#     time.sleep(0.02)

def randomize_colors():
    for x in range(W):
        for y in range(H):
            # print(x,y)
            trellis.color(x, y, (randint(2,10),randint(2,10),randint(2,10)))
    for ts in trellis._trelli:
        for t in ts:
            t.pixels.show()
    time.sleep(1)