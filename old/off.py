from threading import Thread
from board import SCL, SDA, D13, D6
import busio
import digitalio
from adafruit_neotrellis.neotrellis import NeoTrellis
from adafruit_neotrellis.multitrellis import MultiTrellis
from time import sleep

H = 8
W = 32
i2c_bus = busio.I2C(SCL, SDA)

trellis_addresses = [[0x31, 0x30, 0x2f, 0x2e, 0x36, 0x37, 0x38, 0x39],
                     [0x35, 0x34, 0x33, 0x32, 0x3a, 0x3c, 0x3b, 0x3d]]

trelli = [ [], []]

for x, slice in enumerate(trellis_addresses):
    for y, addr in enumerate(slice):
        print(addr)
        t = NeoTrellis(i2c_bus, False, addr=addr)
        t.pixels.auto_write = False
        trelli[x].append(t)
        sleep(0.1)
trellis = MultiTrellis(trelli)
led_matrix = [[(0, 0, 0) for x in range(W)] for y in range(H)]

import time
for x in range(W):
    for y in range(H):
        trellis.color(x, y, (0,0,0))
for ts in trellis._trelli:
    for t in ts:
        t.pixels.show()
print("Done")
