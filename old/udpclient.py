import socket
import sys
from random import randint

HOST, PORT = "localhost", 6666
data = ({
        'grid': [
            (randint(0,31),randint(0,7),randint(0,10),randint(0,10),randint(0,10)),
            (randint(0,31),randint(0,7),randint(0,10),randint(0,10),randint(0,10)),
            (randint(0,31),randint(0,7),randint(0,10),randint(0,10),randint(0,10)),
            (randint(0,31),randint(0,7),randint(0,10),randint(0,10),randint(0,10)),],
        'encoders': [
            (0,randint(0,255),randint(0,255),randint(0,255)),
            (1,randint(0,255),randint(0,255),randint(0,255)),]
            # (2,randint(0,100),randint(0,100),randint(0,100)),
            # (3,randint(0,100),randint(0,100),randint(0,100))]
    })


sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# As you can see, there is no connect() call; UDP has no connections.
# Instead, data is directly sent to the recipient via sendto().
sock.sendto(bytes(str(data) + "\n", "utf-8"), (HOST, PORT))
received = str(sock.recv(1024), "utf-8")

print("Sent")
print("Received: {}".format(received))