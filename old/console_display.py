from time import sleep
from sty import bg, fg, rs
from random import randint


W = 32
H = 8

def draw_grid():
    grid_x = 3
    grid_y = 3
    o = '▓▓ '
    # o = ' ⬤ '
    l = ""
    l += '-'*(W*3+3) + '\n'
    for y in range(H):
        l += '| '
        for x in range(W):
            r,g,b = int(randint(0,100)/10*20),int(randint(0,100)/10*20),int(randint(0,100)/10*20)
            l += str(fg(r,g,b,) + o + rs.fg + rs.bg,)
        l += ' |'
        l += ('\n')

    l += '-'*(W*3+3)
    print(l)    
draw_grid()