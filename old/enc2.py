import smbus2
import RPi.GPIO as GPIO
from time import sleep
import i2cEncoderLibV2



GPIO.setmode(GPIO.BCM)
bus = smbus2.SMBus(1)
INT_pin = 4
GPIO.setup(INT_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

encoder1 = i2cEncoderLibV2.i2cEncoderLibV2(bus, 0x0c)
encoder2 = i2cEncoderLibV2.i2cEncoderLibV2(bus, 0x04)

encconfig = (i2cEncoderLibV2.INT_DATA | i2cEncoderLibV2.WRAP_ENABLE | i2cEncoderLibV2.DIRE_RIGHT | i2cEncoderLibV2.IPUP_ENABLE | i2cEncoderLibV2.RMOD_X1 | i2cEncoderLibV2.RGB_ENCODER)
encoder1.begin(encconfig)
encoder2.begin(encconfig)



encoder1.autoconfigInterrupt()
encoder2.autoconfigInterrupt()
print ('Board ID code: 0x%X' % (encoder1.readIDCode()))
print ('Board Version: 0x%X' % (encoder2.readVersion()))

encoder1.writeRGBCode(0xFF0000)
encoder2.writeRGBCode(0xFF0000)
sleep(0.6)
encoder1.writeRGBCode(0x00FF00)
encoder2.writeRGBCode(0x00FF00)
sleep(0.6)
encoder1.writeRGBCode(0x0000FF)
encoder2.writeRGBCode(0x0000FF)
sleep(0.6)
encoder1.writeRGBCode(0xFFFF00)
encoder2.writeRGBCode(0xFFFF00)
sleep(0.6)
encoder1.writeRGBCode(0x00FFFF)
encoder2.writeRGBCode(0x00FFFF)
sleep(0.6)
encoder1.writeRGBCode(0xFF00FF)
encoder2.writeRGBCode(0xFF00FF)
sleep(0.6)
encoder1.writeRGBCode(0x00)
encoder2.writeRGBCode(0x00)
