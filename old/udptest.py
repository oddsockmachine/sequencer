from socketserver import UDPServer, ThreadingUDPServer, BaseRequestHandler


class myHandler(BaseRequestHandler):
    def handle(self):
        data = self.request[0].strip()
        socket = self.request[1]
        print("{} wrote:".format(self.client_address[0]))
        print(data)
        socket.sendto(data.upper(), self.client_address)


with UDPServer(('localhost', 6666), myHandler) as server:
    server.serve_forever()



