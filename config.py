# Internal server addresses
grid_server_address = ('localhost', 6000)
encoder_server_address = ('localhost', 6001)
screen_server_address = ('localhost', 6002)
encoder_return_address = ('localhost', 6003)
button_server_address = ('localhost', 6004)
dial_server_address = ('localhost', 6005)
# Height and width of grid
H = 8
W = 32

encoder_i2c_addresses = {
    0: 0x04,
    1: 0x0c,
    # 2: 0x03,
    # 3: 0x04,
}

# trellis_addresses = [[0x31, 0x30, 0x2f, 0x2e],
#                      [0x35, 0x34, 0x33, 0x32],
#                      [0x36, 0x37, 0x38, 0x39],
#                      [0x3a, 0x3c, 0x3b, 0x3d]]

trellis_i2c_addresses = [[0x31, 0x30, 0x2f, 0x2e, 0x36, 0x37, 0x38, 0x39],
                         [0x35, 0x34, 0x33, 0x32, 0x3a, 0x3c, 0x3b, 0x3d]]