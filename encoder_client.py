from distutils.command.config import config
import socket
from queue import Queue
from json import dumps, loads
from config import grid_server_address, H, W, encoder_i2c_addresses, encoder_server_address, dial_server_address
# from pykka import ThreadingActor
from threading import Thread
from time import sleep
from buses import bus_registry
from color_scheme import select_scheme
from constants import NOTE_ON, NOTE_OFF

class Encoder(Thread):
    def __init__(self, num) -> None:
        Thread.__init__(self, name=f'Enc_{num}')
        self.num = num
        self.color = (0,0,0)
        self.value = 0


class Encoders(Thread):
    def __init__(self) -> None:
        Thread.__init__(self, name='Grid')
        encoders = [Encoder(i) for i in range(len(encoder_i2c_addresses))]
        self.dial_bus = bus_registry.get('encoder_dial_bus')
        self.led_bus = bus_registry.get('encoder_led_bus')
        self.enc_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        # self.enc_sock.setblocking(False)
        # self.col_scheme = select_scheme('default')
        self.keep_running = True

    def run(self):
        while self.keep_running:
            sleep(1/60)
            # Check for new led commands
            # either convert to diffs, or send whole
            # Transmit to grid server
            updates = []
            while not self.led_bus.empty():
                # TODO accumulate all changes into one list
                updates.append(self.led_bus.get())

                # enc_leds = self.led_bus.get()
            self.draw(updates)

        return

    def send(self, f):
        # print(f)
        self.enc_sock.sendto(bytes(dumps(f), "utf-8"), encoder_server_address)
        # received = str(self.enc_sock.recv(1024), "utf-8")
        # if len(received)>3:
        #     button_presses = loads(received)
        #     print(button_presses)
        return

    def draw(self, enc_led_updates):
        # if len(enc_led_updates) > 0:
        msg = {
            'encoders': enc_led_updates
        }
        self.send(msg)
        # for u in enc_led_updates:
        #     e = u[0]
        #     r,g,b = u[1],u[2],u[3]
            # self.render_note_grid(led_grid)
        # self.redraw_diff()
        return
    
    def update_dials(self):
        return



class Dials(Thread):
    def __init__(self) -> None:
        Thread.__init__(self, name='Dials')
        self.dial_bus = bus_registry.get('dial_bus')
        self.keep_running = True
    
    def run(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.bind(dial_server_address)
        while self.keep_running:
            data = sock.recvfrom(1024) # buffer size is 1024 bytes
            msg = loads(data[0].strip())
            for m in msg:
                print(m)
                self.dial_bus.put(m)

