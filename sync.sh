# Listen for file changes in this directory, and immediately sync changes to remote hardware
alias run_rsync='rsync -azih --exclude ".*/" --exclude "venv/" --exclude ".git/" ./ pi@sunrise.local:sequencer && date +”%H:%M:%S”'
run_rsync; fswatch -o . | while read f; do run_rsync; done
