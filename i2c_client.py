import socket
from json import dumps
from config import grid_server_address, encoder_server_address, H, W
from random import randint, random
from time import sleep
print("starting")
grid_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
enc_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

print("sending")


frame0 = ('off', [])

def random_frame():
    return ({
        "grid": [
            (randint(0,W-1),randint(0,H-1),randint(0,10),randint(0,10),randint(0,10)),
            (randint(0,W-1),randint(0,H-1),randint(0,10),randint(0,10),randint(0,10)),
            (randint(0,W-1),randint(0,H-1),randint(0,10),randint(0,10),randint(0,10)),
            (randint(0,W-1),randint(0,H-1),randint(0,10),randint(0,10),randint(0,10)),],
        "encoders": [
            (0,randint(0,255),randint(0,255),randint(0,255)),
            (1,randint(0,255),randint(0,255),randint(0,255)),]
            # (2,randint(0,100),randint(0,100),randint(0,100)),
            # (3,randint(0,100),randint(0,100),randint(0,100))]
    })

for i in range(300):
    f = dumps(random_frame())
    grid_sock.sendto(bytes(f, "utf-8"), grid_server_address)
    enc_sock.sendto(bytes(f, "utf-8"), encoder_server_address)
    received = str(enc_sock.recv(1024), "utf-8")
    if len(received) >3:
        print(received)
    received = str(grid_sock.recv(1024), "utf-8")
    if len(received) >3:
        print(received)
    # sleep(1/40)


grid_sock.sendto(bytes(dumps({'close':True, 'grid': []}), "utf-8"), grid_server_address)
# enc_sock.sendto(bytes(dumps({'close':True, 'grid': []}), "utf-8"), grid_server_address)
print("done")
