import socket as sock
from threading import Thread
import smbus2
import RPi.GPIO as GPIO
import i2cEncoderLibV2
from config import encoder_server_address, H, W, encoder_i2c_addresses, dial_server_address
from socketserver import UDPServer, ThreadingUDPServer, BaseRequestHandler
from json import dumps, loads
# import traceback
from time import sleep#, perf_counter
# from random import randint, random
from queue import Queue

bus = smbus2.SMBus(1)

encoder_action_bus = Queue(100)

# class Dispatcher(object):
#     def __init__(self) -> None:
#         # self.num = num
#         self.q = Queue(100)
#         return
#     # def set_client(self, client):
#     #     self.client = client
#     #     print(client)
#     #     return
#     def call(self, num, cmd):
#         print(f"Encoder {i} called {cmd}")
#         # call client
#         self.q.put((i, cmd))
#         return
#     def get_all(self):
#         batch = []
#         while not self.q.empty():
#             batch.append(self.q.get())
#         return batch

# dispatcher = Dispatcher()

def make_cb(i, cmd):
    def cb():
        encoder_action_bus.put((i, cmd))
        # dispatcher.call(i,cmd)
        return
    return cb

encoders = []
encconfig = (i2cEncoderLibV2.INT_DATA | i2cEncoderLibV2.WRAP_ENABLE | i2cEncoderLibV2.DIRE_RIGHT | i2cEncoderLibV2.IPUP_ENABLE | i2cEncoderLibV2.RMOD_X1 | i2cEncoderLibV2.RGB_ENCODER)
for i, add in encoder_i2c_addresses.items():
    e = i2cEncoderLibV2.i2cEncoderLibV2(bus, add)
    e.begin(encconfig)
    e.writeCounter(0)
    e.writeMax(100)
    e.writeMin(-100)
    e.writeStep(1)
    e.writeAntibouncingPeriod(8)
    e.writeDoublePushPeriod(50)
    e.writeGammaRLED(i2cEncoderLibV2.GAMMA_OFF)
    e.writeGammaGLED(i2cEncoderLibV2.GAMMA_OFF)
    e.writeGammaBLED(i2cEncoderLibV2.GAMMA_OFF)
    e.onButtonPush = make_cb(i, "button_push")
    e.onButtonDoublePush = make_cb(i, "button_double_push")
    e.onIncrement = make_cb(i, "increment")
    e.onDecrement = make_cb(i, "decrement")
    e.autoconfigInterrupt()
    print ('Board ID code: 0x%X' % (e.readIDCode()))
    print ('Board Version: 0x%X' % (e.readVersion()))
    encoders.append(e)


def Encoder_INT():
    for e in encoders:
        e.updateStatus()

# GPIO.setmode(GPIO.BCM)
INT_pin = 4
# GPIO.setup(INT_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
# GPIO.add_event_detect(INT_pin, GPIO.FALLING, callback=Encoder_INT, bouncetime=10)


def update_encoders(msg):
    for e_rgb in msg.get('encoders'):
        enc = encoders[e_rgb[0]]
        rgb = i2cEncoderLibV2.rgb_to_hex(e_rgb[1],e_rgb[2],e_rgb[3])
        enc.writeRGBCode(rgb)
        pass
    if 'query' in msg.items():
        # get changes
        pass
        # socket.sendto(data.upper(), self.client_address)
    return


def encoder_startup_pattern(encoders):
    for c in [0xFF0000, 0x00FF00, 0x0000FF, 0xFFFF00, 0x00FFFF, 0xFF00FF, 0xFFFFFF]:
        encoders[0].writeRGBCode(c)
        encoders[1].writeRGBCode(c)
        sleep(0.1)



class myHandler(BaseRequestHandler):
    def setup(self):
        return
    def handle(self):
        msg = loads(self.request[0].strip())
        Encoder_INT()
        update_encoders(msg)  # act on the data
        return

def runserver():
    encoder_startup_pattern(encoders)
    with UDPServer(encoder_server_address, myHandler) as server:
        print(f"Encoder server started: {server}")
        server.serve_forever()

if __name__ == '__main__':
    server_thread = Thread(target=runserver, daemon=True)
    server_thread.start()
    print(f"thread started: {server_thread.name}")
    dial_sock = sock.socket(sock.AF_INET, sock.SOCK_DGRAM)

    while True:
        Encoder_INT()
        if not encoder_action_bus.empty():
            updates = []
            while not encoder_action_bus.empty():
                updates.append(encoder_action_bus.get())
            dial_sock.sendto(bytes(dumps(updates), "utf-8"), dial_server_address)
        sleep(0.1)

