from queue import Queue
# from pykka import ActorRegistry
from constants import debug

# midi_in_bus = Queue(100)
# midi_out_bus = Queue(200)
# ticker_bus = Queue(100)
# clock_bus = Queue(100)

# OLED_bus = Queue(100)
# grid_button_bus = Queue(100)
# grid_led_bus = Queue(100)
# encoder_dial_bus = Queue(100)
# encoder_led_bus = Queue(100)

def clear_queue(q):
    while not q.empty():
        q.get()
    return

class Bus_Registry(object):
    def __init__(self):
        self.registry = {}
    def get(self, name) -> Queue:
        return self.registry[name]
    def add(self, name, bus):
        self.registry[name] = bus

bus_registry = Bus_Registry()
# bus_registry.add('midi_in_bus', midi_in_bus)
# bus_registry.add('midi_out_bus', midi_out_bus)
# bus_registry.add('ticker_bus', ticker_bus)
# bus_registry.add('clock_bus', clock_bus)
# bus_registry.add('OLED_bus', OLED_bus)
# bus_registry.add('grid_button_bus', grid_button_bus)
# bus_registry.add('grid_led_bus', grid_led_bus)
# bus_registry.add('encoder_dial_bus', encoder_dial_bus)
# bus_registry.add('encoder_led_bus', encoder_led_bus)

for q in 'midi_in midi_out ticker clock OLED grid_button grid_led encoder_dial dial encoder_led'.split(' '):
    # b = q +
    bus_registry.add(q+"_bus", Queue(300))

# actor_registry = ActorRegistry

# def proxy_registry(name):
#     return actor_registry.get_by_class_name('OLED_Screens')[0].proxy()