from distutils.command.config import config
import socket
from queue import Queue
from json import dumps, loads
from config import grid_server_address, H, W, button_server_address
# from pykka import ThreadingActor
from threading import Thread
from time import sleep
from buses import bus_registry
from color_scheme import select_scheme
from constants import NOTE_ON, NOTE_OFF

class Grid(Thread):
    def __init__(self) -> None:
        Thread.__init__(self, name='Grid')
        self.H = H
        self.W = W
        self.refresh_counter = 0
        self.button_bus = bus_registry.get('grid_button_bus')
        self.led_bus = bus_registry.get('grid_led_bus')
        self.led_matrix = [[(0, 0, 0) for x in range(H)] for y in range(W)]
        self.old_led_matrix = [[(0, 0, 0) for x in range(H)] for y in range(W)]
        self.grid_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        # self.grid_sock.setblocking(False)
        self.col_scheme = select_scheme('default')
        self.keep_running = True

    def run(self):
        while self.keep_running:
            sleep(1/1000)
            # Check for new led commands
            # either convert to diffs, or send whole
            # Transmit to grid server
            while not self.led_bus.empty():
                led_grid = self.led_bus.get()
                self.draw(led_grid)
            else:
                msg = {'grid': []}
                self.send(msg)
            button_updates = []
            while not self.button_bus.empty():
                button_updates.append(self.button_bus.get())
        return

    def send(self, f):
        # print(f)
        self.grid_sock.sendto(bytes(dumps(f), "utf-8"), grid_server_address)
        # received = str(self.grid_sock.recv(1024), "utf-8")
        # if len(received)>3:
        #     button_presses = loads(received)
        #     print(button_presses)
        #     for b in button_presses:
        #         print(b)
        #         self.button_bus.put(b)
        return
    def calc_diff(self):
        return
    def update(self, matrix):
        return


    def draw(self, led_grid):
        self.render_note_grid(led_grid)
        self.redraw_diff()
        return

    def redraw_diff(self):
        self.refresh_counter += 1
        diffs = []
        for x in range(len(self.led_matrix)):
            for y in range(len(self.led_matrix[x])):
                if self.led_matrix[x][y] != self.old_led_matrix[x][y]:
                    c = self.led_matrix[x][y]
                    diffs.append((x, y, c[0], c[1], c[2],))
                self.old_led_matrix[x][y] = self.led_matrix[x][y]
        if self.refresh_counter == 60:
            msg = {'grid': diffs}
            self.send(msg)
            # send full refresh
            # self.send(self.led_matrix)
        else:
            # Send diff
            msg = {'grid': diffs}
            self.send(msg)
        return

    def render_note_grid(self, led_grid):
        for x in range(len(led_grid)):
            for y in range(len(led_grid[x])):
                color = self.col_scheme.get_color(led_grid[x][y], x, y)
                # col = c.PALLETE[led_grid[x][y]]
                self.led_matrix[x][H-1-y] = color
        return

class Buttons(Thread):
    def __init__(self) -> None:
        Thread.__init__(self, name='Buttons')
        self.button_bus = bus_registry.get('grid_button_bus')
        self.keep_running = True
    
    def run(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.bind(button_server_address)
        while self.keep_running:
            data = sock.recvfrom(1024) # buffer size is 1024 bytes
            msg = loads(data[0].strip())
            for m in msg:
                self.button_bus.put(m)

