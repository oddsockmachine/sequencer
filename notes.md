installation
    sudo apt-get install git
    curl -fsSL https://tailscale.com/install.sh | sh
    sudo tailscape up
    sudo raspi-config nonint do_i2c 0
    lsmod | grep i2c
    sudo apt-get install i2c-tools
    sudo i2cdetect -y 1
    sudo apt-get install libasound2-dev
    sudo apt-get install libjack-dev

Modes:

8x32 step sequencer
    adjustable bpm
    step mode, scroll/scrub with encoder

vcv-style sequencer, where each cell has note/velocity set by encoder
    allows for multiple sequencers on one page

Scene manager
    scroll/step through scenes with adjustable cc values

Drum/beat sequencer
    Per-drum randomization

4-way scene/step sequencer
    stepped/scrolled by 4x encoders

Implementation
    local server for i2c devices, like fadecandy
    could be modular, so runs as a thread in production (likely higher performance), separate server while developing

TODO
    x rearrange boards, make x/y consistent across server and client
    every x seconds/frames, send a full update to avoid drift
    color scheme should be a filter/chainable which can be adjusted live (brightness, theme, gradient, etc) (steal chainable from sunrise repo)
    use interrupt for encoders
    return button/dial input back via client/server comms?